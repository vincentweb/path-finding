#!/usr/bin/env python

from Tkinter import *
import tkFileDialog
import scipy.io as sio

class Editeur_map:

    def __init__(self):
        self.root = Tk()
        self.c = Canvas(self.root, bg="white",width=700, height=700)
        self.list = []
        self.__maz()
        self.c.bind("<Button1-Motion>", self.__drawPixel)
        self.c.bind("<Button-1>", self.__drawPixel)
        self.c.bind("<Button3-Motion>", self.__erasePixel)
        self.c.bind("<Button-3>", self.__erasePixel)

        self.coords = StringVar()
        self.coords.set('i=, j=')
        self.label_coords = Label(self.root, textvariable=self.coords, fg='blue')
        self.label_coords.pack()
        self.c.pack()

        Button(self.root, text="Save", command=self.__save).pack(side=LEFT)
        Button(self.root, text="Load", command=self.__load).pack(side=LEFT)
        Button(self.root, text="RaZ", command=self.__maz).pack(side=LEFT)

    def __drawPixel(self, event):
        x_homogene = event.x - event.x%10
        y_homogene = event.y - event.y%10
        self.c.create_rectangle(x_homogene, y_homogene, x_homogene + 10, y_homogene+10, fill="black", width=0)
        if (20 <= x_homogene <= 680 and 20 <= y_homogene <= 680):
            self.list[y_homogene/10][x_homogene/10] = 7
        self.coords.set('i='+str(y_homogene/10)+', j='+str(x_homogene/10))

    def __erasePixel(self, event):
        x_homogene = event.x - event.x % 10
        y_homogene = event.y - event.y % 10
        self.c.create_rectangle(x_homogene, y_homogene, x_homogene + 10, y_homogene+10, fill="white")
        if (20 <= x_homogene <= 680 and 20 <= y_homogene <= 680):
            self.list[y_homogene/10][x_homogene/10] = 1
        self.coords.set('i='+str(y_homogene/10)+', j='+str(x_homogene/10))

    def __save(self):
        filename = tkFileDialog.asksaveasfilename()
        if filename != "":
            sio.savemat(filename, {'scene' : self.list})

    def __load(self):
        filename = tkFileDialog.askopenfilename(**{'defaultextension' : '.mat'})
        if filename != "":
            self.__maz()
            mat = sio.loadmat(filename)
            self.list = mat['scene']
            for y in range(70):
                for x in range(70):
                    if self.list[y][x] == 7:
                        self.c.create_rectangle(x * 10, y * 10, x * 10 + 10, y * 10 + 10, fill="black")

    def __maz(self):
        self.c.delete(ALL)
        self.list = []
        for y in range(70):
            l = []
            for x in range(70):
                if (x == 0 or x == 1 or y == 0 or y == 1 or x == 68 or y == 68 or x == 69 or y == 69):
                    l.append(7)
                else:
                    l.append(1)
            self.list.append(l)

        for y in range(700):
            if y % 10 == 0:
                self.c.create_line(0, y, 700, y)
                for x in range(700):
                    if x % 10 == 0:
                        self.c.create_line(x, 0, x, 700)
                        if (x == 0 or x == 10 or y == 0 or y == 10 or x == 680 or y == 680 or x == 690 or y == 690):
                            self.c.create_rectangle(x, y, x + 10, y + 10, fill="black", width=0)

    def loop(self):
        self.root.mainloop()

em = Editeur_map()
em.loop()
