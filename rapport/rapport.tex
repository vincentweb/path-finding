\documentclass[12pt,a4paper]{article}
\usepackage[utf8x]{inputenc}
\usepackage{ucs}
\usepackage[french]{babel}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{subfig}

\author{\textsc{Angladon}\\Vincent \and \textsc{Babin}\\Guillaume}
\title{Projet Recherche opérationnelle \\ Navigation robotique}
\date{8 juin 2012}

\begin{document}

\maketitle

\begin{abstract}
  L'objectif de ce projet était de concevoir un programme permettant
  de calculer la trajectoire optimale d'un robot, entre deux points
  d'une pièce encombrée d'obstacles fixes. L'ensemble des positions et
  tailles étant connus a priori, et fournis dans une matrice.
\end{abstract}

\tableofcontents

\newpage
\section{Conception}

Tout d'abord, nous avons choisi de développer notre programme en
\textsc{Matlab} afin de profiter de la fonction de squelettisation
fournie, et des diverses fonctions de haut niveau disponibles dans ce
logiciel pour l'affichage de matrices (la scène), de lignes (le
graphe) et de menus.  Nous allons ici détailler les étapes de calcul
du chemin optimal du robot, et préciser ainsi les choix techniques que
nous avons pris.

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=0.6\textwidth]{illustrations/obstacles-scene}}
  \caption{Scène Obstacles}
\end{figure}

\subsection{Calcul de la matrice des positions possibles}
Cette matrice, de taille identique à la scène, est composée de
booléens. Elle indique les pixels sur lesquels peut se situer le
centre du robot, compte tenu de sa taille et des obstacles. Son calcul
(qui n'est effectué que si la taille du robot est supérieure à 1) est
une étape préliminaire. Cette matrice sert par la suite de
filtre.  Pour déterminer les éléments, on procède ainsi : on parcourt
tous les pixels de la scène qui ne sont pas des murs et pour chacun,
on regarde s'il y a un mur à une distance plus petite que le rayon.

\subsection{Calcul du squelette}
Nous utilisons pour cette étape la fonction fournie de squelettisation
par calcul d'une fonction discriminante sur un voisinage de 8 points.

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=0.6\textwidth]{illustrations/obstacles-squelette}}
  \caption{Squelette}
\end{figure}

\subsection{Élagage du squelette}
Dans cette étape, on filtre le squelette à l'aide de la matrice des
positions possibles pour ne garder que des pixels possibles sur
squelette.

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=0.6\textwidth]{illustrations/obstacles-squelette-filtre}}
  \caption{Squelette filtré}
\end{figure}

\subsection{Raccordement du départ et de l'arrivée}
Afin de raccorder les points de départ et d'arrivée de façon optimale
au squelette, on commence par calculer les distances entre ces points
et chacun des points du squelette. Puis on trie les points du
squelette par distance au point de départ. Ensuite on regarde si la
ligne de pixels qui relie ce point au point de départ est bien
entièrement contenue dans la matrice des positions possibles, auquel
cas on ajoute ces pixels au squelette, sinon on passe au point
suivant. On fait de même pour le point d'arrivée.

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=0.6\textwidth]{illustrations/obstacles-squelette-raccorde}}
  \caption{Squelette raccordé au départ et à l'arrivée}
\end{figure}

\subsection{Post-squelettisation}
L'objectif de cette étape est d'analyser le squelette pour détecter
les carrefours et les extrémités (qui seront plus tard les nœuds du
graphe). Pour cela, on regarde, pour chaque pixel du graphe, combien
de ses huit voisins sont sur le squelette.

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=0.6\textwidth]{illustrations/obstacles-squelette-post}}
  \caption{Squelette analysé}
\end{figure}

\newpage

\subsection{Calcul du graphe}
Dans cette étape nous générons un graphe à partir du squelette dont
les arcs représentent les chemins que peut parcourir le robot.  Comme
les arcs sont rarement rectilignes, nous insérons des nœuds à chaque
changement de direction.  La longueur d'un arc peut être ainsi calculée
en prenant la distance euclidienne entre ses deux nœuds extrémaux.
Cela évite de devoir calculer la longueur exacte d'arcs curvilignes.

Le calcul du graphe s'effectue ainsi :
\begin{itemize}
\item on part du pixel de départ qui devient le pixel d'étude
\item on détermine tous les voisins du pixel d'étude
\item on place les voisins du pixel en amont de notre liste de pixels
  à étudier de sorte à parcourir le squelette en profondeur. Dit
  autrement, les pixels étudiés consécutivement appartiendront au même
  chemin.
\item on vérifie que tous les pixels du chemin que l'on est en train
  de visiter via notre parcours en profondeur sont globalement
  alignés. Si ce n'est pas le cas, on insère un nœud dans notre
  graphe et on ré-initialise le chemin que l'on construisait.
\item on étudie de nouveaux pixels jusqu'à que notre liste de pixels à
  étudier soit vide.
\end{itemize}


\subsection{Calcul des changements de direction}
Le problème ici consiste à déterminer si une liste de pixels consitue
une ligne rectiligne ou pas.  On commence par appliquer une rotation à
tous les points constitués des centres de gravité des pixels de notre
ligne afin que le vecteur défini par les pixels extrémaux soit
colinéaire avec le vecteur unitaire de l'axe $(Ox)$.  Si la valeur
absolue de l'ordonnée d'un des points auxquels on a appliqué la
rotation dépasse une valeur seuil, alors on considère cette ligne
comme non rectligne.


\subsection{Calcul du plus court chemin}
Le calcul du plus court chemin dans le graphe s'effectue en utilisant
l'algorithme de Moore-Dijkstra comme précisé dans l'énoncé.

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=0.6\textwidth]{illustrations/obstacles-graphe-chemin}}
  \caption{Graphe avec le chemin optimal en rouge}
\end{figure}

\newpage
\subsection{Résultat final}

Comme on peut le constater sur la figure~\ref{final} (et mieux encore
sur la figure~\ref{base}), notre algorithme de construction du graphe
peut créér plusieurs nœuds autour d'un carrefour. Nous avons conservé
ce comportement car il aboutit à une trajectoire moins anguleuse, plus
lissée.

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=0.9\textwidth]{illustrations/obstacles-tout}}
  \caption{Affichage final du résultat}
  \label{final}
\end{figure}

\newpage
\section{Résultat}

\subsection{L'interface graphique}

Afin de faciliter l'utilisation du programme et de pouvoir tester
facilement différentes scènes avec différents paramètres, nous avons
ajouter une interface graphique simple qui permet à l'utilisateur de
modifier les réglages aisément.

\begin{figure}[h!]
  \subfloat[Choix de la scène]{%
    \begin{minipage}[c][10cm]{%
        0.5\textwidth}
      \centering%
      \includegraphics[scale=0.8]{illustrations/menu}
  \end{minipage}}
  \subfloat[Choix des paramètres]{%
    \begin{minipage}[c][6.5cm]{%
        0.5\textwidth}
      \centering%
      \includegraphics[scale=0.8]{illustrations/param}
  \end{minipage}}
  \caption{Interface graphique}
\end{figure}

\newpage

De même, lorsqu'il n'est pas possible de relier le départ et l'arrivée
(graphe non connexe), l'utilisateur est informé via une boîte de
dialogue.

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=1.2\textwidth]{illustrations/erreur}}
  \caption{Gestion des erreurs}
\end{figure}

\subsection{L'affichage du chemin trouvé}

Afin de ne pas noyer l'utilisateur sous un flot de fenêtres, nous
avons décidé de concentrer l'information du résultat sur une unique
figure, qui suit cette charte graphique :
\begin{itemize}
\item pixels en bleu foncé : les obstacles et murs extérieurs
\item pixels en bleu clair : le squelette filtré (ne comportant que
  les points possibles)
\item pixels en rouge clair : les sommets du graphe
\item pixels en jaune foncé : les carrefours du squelette
\item pixels en jaune clair : les extrémités du graphe (sauf le départ
  et l'arrivée, quit sont des sommets du graphe)
\item lignes noires : les arêtes du graphe ne faisant pas partie du
  chemin
\item lignes vertes : les arêtes du graphe faisant partie du chemin
\item pixels en rouge foncé : le fond (pixels libres)
\end{itemize}

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=0.9\textwidth]{illustrations/scene-fournie}}
  \caption{Résultat final}
  \label{base}
\end{figure}

\clearpage
\section{Tests}

Nous avons effectué certains tests unitaires pour valider nos
algorithmes.

Pour nos tests d'intégration, nous avons utilisé un éditeur de scène
graphique en Python, écrit originellement par Jérémy \textsc{Rivière},
que nous avons adapté. Celui-ci nous a permis de créer et tester un
nombre important de scènes.

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=0.8\textwidth]{illustrations/labyrinthe}}
  \caption{Scène Labyrinthe court}
\end{figure}

\begin{figure}[h!]
  \makebox[\textwidth]{
    \includegraphics[width=2\textwidth]{illustrations/lab}}
  \caption{Scène Labyrinthe long}
\end{figure}

\clearpage

\section{Conclusion}

Ce projet fut intéressant dans la mesure où il nous a permis
d'appliquer de manière visuelle certains éléments vus en cours de
Recherche opérationnelle, ainsi que d'aborder le traitement d'images.

Nous regrettons cependant le choix de Matlab implicitement imposé pour
ce projet puisque les fournitures étaient écrites en Matlab.  D'une
part, Matlab est peu adapté aux manipulations non matricielles
(structure de graphe autre que matrice d'adjacence, parcours par
files, \ldots). D'autre part, les étudiants ne bénéficiant pas de
licences Matlab, il est compliqué de travailler hors de l'école.
\end{document}
