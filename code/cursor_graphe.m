function txt = cursor_graphe(empt, event_obj)
% Customizes text of data tips for graphs

global g;


pos = get(event_obj,'Position');
pos = [pos(2) pos(1)]; % i <-> y et j<-> x

dist   = '';
fils   = '';
indice = '';
for i=1:length(g)
    if g(i).pos == pos
        dist = num2str(g(i).distance);
        indice = num2str(i);
        fils = num2str(g(i).fils);
    	break;
    end
end
%i

if strcmp(dist,'')
    txt = {['i: '     , num2str(pos(1))], ...
           ['j: '     , num2str(pos(2))], ...
          };
    
else
    txt = {['i: '     , num2str(pos(1))], ...
           ['j: '     , num2str(pos(2))], ...
           ['indice: ', indice]         , ...
           ['dist: '  , dist]           , ... 
           ['fils: '  , fils]           , ...
          };
end

