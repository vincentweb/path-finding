function [ squel ] = raccordement( squel, depart, arrivee, accessibilite )

    squel(depart(1), depart(2)) = 0;
    squel(arrivee(1), arrivee(2)) = 0;
    
    % points du squelette [indice_ligne, indice_colonne, distance_depart, distance_arrivee]
    [row, col] = find(squel ~= 7);
    nb_points = size(row,1);
    points = [row col zeros(nb_points,2)];
    
    % calcul des distances
    for i=1:nb_points
        points(i, 3) = distance(points(i, 1:2), depart);
        points(i, 4) = distance(points(i, 1:2), arrivee);
    end
    
    % calcul raccordement depart
    points = sortrows(points, 3);
    continuer = true;
    j = 2;
    while continuer && j <= nb_points
        [ x , y ] = bresenham(depart(1), depart(2), points(j,1), points(j,2));
        dans_mur = false;
        for i=1:size(x,1)
           if ~accessibilite(x(i), y(i))
               dans_mur = true;
               break
           end
        end
        if ~dans_mur % si le chemin est possible, on l'ajoute au squelette
            for i=1:size(x,1)
               squel(x(i),y(i)) = 0; 
            end
            continuer = false;
        end
        j = j + 1;
    end
    
    % calcul raccordement arrivee
    points = sortrows(points, 4);
    continuer = true;
    j = 2;
    while continuer && j <= nb_points
        [ x , y ] = bresenham(points(j,1), points(j,2), arrivee(1), arrivee(2));
        dans_mur = false;
        for i=1:size(x,1)
           if ~accessibilite(x(i), y(i))
               dans_mur = true;
               break
           end
        end
        if ~dans_mur % si le chemin est possible, on l'ajoute au squelette
            for i=1:size(x,1)
               squel(x(i),y(i)) = 0; 
            end
            continuer = false;
        end
        j = j + 1;
    end
end

