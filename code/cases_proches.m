function [ cases ] = cases_proches( i_c, j_c, scene, distance_limite )
% cases : vecteur contenant le contenu des cases

    taille_i = size(scene, 1);
    taille_j = size(scene, 2);

    cases = [];
    
    distance_limite_entiere = ceil(distance_limite);
    
    i_min = max(1, i_c - distance_limite_entiere);
    i_max = min(taille_i, i_c + distance_limite_entiere);
    
    j_min = max(1, j_c - distance_limite_entiere);
    j_max = min(taille_j, j_c + distance_limite_entiere);
    
    for i = i_min:i_max
        for j = j_min:j_max
            if distance([i j], [i_c, j_c]) <= distance_limite
                cases = [cases ; scene(i,j)];
            end
        end
    end
end

