clear

global g; % le graphe

prompts = {'taille_robot', 'depart_x', 'depart_y', 'arrivee_x', 'arrivee_y'};
defaults = {'2', '6', '5', '18', '15'};

figure     = menu('QUELLE SCENE ?'        , ...
                  'Blocus'                , ...  
                  'Classique'             , ...  
                  'Classique + variante'  , ...
                  'Labyrinthe court'      , ...
                  'Labyrinthe long'       , ...
                  'Miettes'               , ...
                  'N7'                    , ...
                  'Obstacles'             , ...
                  'Perspective'           , ...
                  'Plus'                  , ...
                  'Tunnel'                   ...
                );
display_graphe = true;
            
switch figure
    case 1,
		load('../scenes/blocus.mat');
        defaults = {'1', '35', '35', '11', '35'};
	case 2,
        load('../scenes/scene_chambres.mat');
        scene = imscene;
	case 3,
		load('../scenes/scene_chambres_2.mat');
        scene = imscene;
    case 4,
        load('../scenes/lab2.mat');
        defaults = {'1', '5', '5', '63', '62'};
    case 5,
		load('../scenes/lab.mat');
        defaults = {'1', '13', '7', '509', '517'};
        display_graphe = false;
	case 6,
		load('../scenes/miettes.mat');
        defaults = {'1', '44', '7', '24', '50'};
	case 7,
		load('../scenes/n7.mat');
        defaults = {'2', '35', '17', '20', '47'};
    case 8,
		load('../scenes/obstacles.mat');
        defaults = {'7', '63', '7', '7', '63'};
	case 9,
		load('../scenes/perspective.mat');
        defaults = {'0.5', '4', '4', '28', '39'};
	case 10,
		load('../scenes/plus.mat');
        defaults = {'4', '8', '8', '50', '62'};
    case 11,
		load('../scenes/tunnel.mat');
        defaults = {'9', '10', '33', '49', '33'};
end



scene = int16(scene);

rep = inputdlg(prompts,'Parametres',1,defaults);
for i = 1:length(rep)
    rep{i} = str2num(rep{i});
end



% configurations
taille_robot = rep{1};
depart = [rep{2} rep{3}];
arrivee = [rep{4} rep{5}];


if taille_robot > 1
    % calcul matrice des positions possibles
    display 'calcul matrice des positions possibles ...'
    positions_possibles = calcul_positions_possibles(scene, taille_robot);
else
    positions_possibles = scene-7;
end

% calcul du squelette
display 'calcul du squelette ...'
squel = squelette(scene);

% elagage du squelette
display 'elagage du squelette ...'
squel(positions_possibles == 0) = 7;

% raccordement du depart et de l'arrivee
display 'raccordement du depart et de l''arrivee ...'
squel = raccordement(squel, depart, arrivee, positions_possibles);

% post-squelettisation
display 'post-squelettisation ...'
squel = post_squel(squel);

% calcul du graphe
display 'calcul du graphe ...'
[g, squel, noeud_depart, noeud_arrivee] = squelette_to_graphe(squel, depart, arrivee);

chemin = [];

if noeud_arrivee.indice == -1
    error = 'Impossible de relier l''arriv�e au d�part !';
    display(error);
    errordlg(error);
else
    % Calcul du plus court chemin
    display 'calcul du plus court chemin ...';
    [chemin, g] = dijkstra(g, noeud_depart, noeud_arrivee);
end
    
fig = figure; 
datacursormode on;
imagesc(squel - 1.5*scene);
hold all
axis equal

if display_graphe
    display('Affichage du graphe');
    plot_graph(g);
end

display('Affichage du chemin');
lignes = [];
for i=1:length(chemin)-1
    p = plot([chemin(i).pos(2), chemin(i+1).pos(2)], [chemin(i).pos(1), chemin(i+1).pos(1)]);
    set(p,'Color','green','LineWidth',3)
    hold all
end

