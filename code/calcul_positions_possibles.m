% renvoie les positions possibles du centre du robot compte tenu de sa
% taille
function [ positions_possibles ] = calcul_positions_possibles( scene, taille )

    t_i = size(scene,1);
    t_j = size(scene,2);
    
    n_total = t_i * t_j;
    n = 0;
    
    h = waitbar(0, 'Calcul de la matrice d''accessibilite');
    
    positions_possibles = int16(zeros(t_i, t_j));
    
    distance = taille / 2 + 1/sqrt(2); % rayon + diagonale/2
    
    for i = 1:t_i
        for j = 1:t_j
            n = n + 1;
            waitbar(n/n_total, h)
            if scene(i,j) ~= 7 % si la case n'est pas un mur (7=mur)
                cases = cases_proches(i, j, scene, distance);
                res = (size(find(cases == 7),1) == 0); % (7=mur)
                positions_possibles(i,j) = res;
            end
        end
    end
    
    close(h)
end

