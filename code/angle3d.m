function [angle,axis]=angle3d(v1, v2, normal)
    if nargin ==2
        normal = cross(v1, v2);
    end
    if abs(normal) < [0.0001 0.0001 0.0001] == [1 1 1]
        %v1,v2,normal
        angle = 0;
        axis = [0 0 1];
        return;
    end
    prodNorm = norm(v1)*norm(v2);
     if prodNorm~=0
        cos = dot(v1, v2)/prodNorm;
    else
        cos = 0;
    end
    axis = cross(v1, v2);
    norme = norm(axis);
    if norme~=0
        axis = axis/norme;
    end
    sign = dot(normal, axis);

    if (cos>1) || (cos< -1)
        display('ERROR ANGLE3D')
        v1, v2
        angle =  0;
    else
        angle = acos(cos);
    end
    if sign < 0
        angle = -angle;
    end
return;