function [ dist ] = distance( pointA , pointB )

    dist = sqrt(sum((pointA - pointB) .^ 2));

end