function [ im ] = colorise_chemin(im, chemin)

    % colorise les noeuds de dijkstra
    for i=1:length(chemin)-1
        %chemin(i)
        [x y] = bresenham(chemin(i).pos(1), chemin(i).pos(2), chemin(i+1).pos(1), chemin(i+1).pos(2));
        z = x + (y-1) * size(im,1);
        im(z) = 5;
        im(z(1)) = 6;
        im(z(length(z))) = 6;
    end
    
end