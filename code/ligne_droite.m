% Renvoie true si la ligne est droite
function [est_droite] = ligne_droite(ligne)
   
    n         = size(ligne,1);
    origine   = ligne(1,:);
    seuil = 0.5; %tolérance
    
    est_droite = true;
    
    % espace affine -> espace vectoriel
    
    ligne   = ligne - ones(size(ligne, 1), 1) * origine;
    
    extremite = ligne(n,:);
    angle     = -1 * angle3d([1 0 0], [(extremite) 0], [0 0 1]);
    
    mat_rot   = [cos(angle) -sin(angle) ; sin(angle) cos(angle)];
    
    % on applique une rotation sur la ligne pour qu'elle soit horizontale
    ligne  = (mat_rot * ligne')';
    
    vire_a_droite = find(ligne(:,2) > seuil );
    vire_a_gauche = find(ligne(:,2) < -seuil );
    
    
    
    if length(vire_a_droite) > 0 || length(vire_a_gauche) > 0 
        est_droite = false;
    end
        
            
return;