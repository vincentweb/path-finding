% -- Entree
% im     : l'image squeletisee
% depart : le point de depart du robot et de l'algo, car ca serait moche de
%           commencer au milieu d'une ligne qui se retrouverait ainsi coupee en deux

% -- Sortie :
% g : graphe
% im : nouveau squelette/image ou on a colore les noeuds

function [g, im, noeud_depart, noeud_arrivee] = squelette_to_graphe(im, depart, arrivee)
    function [vois] = voisins(im, point)
        voisinage = im(point(1)-1 : point(1)+1, point(2)-1 : point(2)+1);
        voisinage(2,2) = 7; % pour pas retrouver 'point' dans les voisins
        [vx, vy] = find(voisinage ~= 7);
        vois = [vx+point(1)-2 vy+point(2)-2];
    end

    visited = zeros(size(im));
    is_node = zeros(size(im));
    to_see = [depart 0];
    g = [];
    ligne = [];
    
    noeud_arrivee = struct('indice', -1, 'pos', arrivee, 'fils', [], 'distance', Inf, 'visite', false);
    
    % Tant qu'on a pas parcouru tous les points du squelette
    while ~isempty(to_see)  %%length(to_see) > 0
        %%to_see(1,:);
        point = to_see(1,1:2);
        ipere = to_see(1,3);
        to_see = to_see(2:size(to_see),:); % point = to_see.pop(0)
        
        if ~visited(point(1), point(2))
            %[point ipere]
            ligne = [ligne ; point];
            visited(point(1), point(2)) = 1;    
            vois = voisins(im, point);

            nb_vois = size(vois,1);
            
            if nb_vois < 1
                point
                display('Oups, je n ai pas de voisin :(');
                %break;
            end
            
            ig = length(g)+1;

            if norm(point - arrivee) < 0.01 || ipere == 0 || ((nb_vois > 2 || nb_vois <= 1  || ~ligne_droite(ligne))   && ~(max(abs(point-g(ipere).pos)) <= 1))
                % c'est un noeud
                ligne = [];
                g = [g  ; struct('indice', ig, 'pos', point, 'fils', [], 'distance', Inf, 'visite', false)];
                is_node(point(1), point(2)) = ig;
                if point == arrivee
                    noeud_arrivee = g(length(g)); % Tres foireux de declarer une var de retour dans un if perdu
                end
                im(point(1), point(2)) = 6; % affichage des noeuds
                if ipere ~= 0
                    g(ig).fils = [ipere];
                    g(ipere).fils = [g(ipere).fils ig];
                end
                ipere = ig;
            %else
                %im(point(1), point(2)) = 0; % affichage des non noeuds
            end
            
            % Le noeud que l'on visite est le pere de tous les voisins
            mig = ipere*ones(size(vois,1),1);
            vois = [vois mig]; % on rajoute l'indice du pere sur la col 3
            to_see = [vois ; to_see]; % parcours en profondeur
        elseif is_node(point(1), point(2)) && is_node(point(1), point(2)) ~= ipere
            ig = is_node(point(1), point(2));
            %g(ig).fils = [g(ig).fils ipere];
            g(ipere).fils = [g(ipere).fils ig];    
        end
    end    
    %%%length(g)
    noeud_depart = g(1);
    
    if length(noeud_depart.fils) < 1
        display('Le noeud de départ n a pas de fils, ça ne marchera jamais :(');
    end
end