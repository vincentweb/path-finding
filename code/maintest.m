% Pour faire des tests rapides sans perdre du temps avec le calcul de la
% matrice d'accéssibilité.


clear
global g;

% configurations
taille_robot = 4;
depart = [6 5];
arrivee = [18 15];

% chargement de la scene
display 'chargement de la scene ...'
load scene_chambres_2

% % Test avec un seul noeud
% imscene = 7 * ones(5,5);
% imscene(3,3) = 0;
% imscene = int16(imscene);
% depart = [3 3];
% arrivee = [3 3];

% calcul matrice d'accessibilite
display 'calcul matrice d''accessibilite ...'
%accessibilite = calcul_accessibilite(imscene, taille_robot);

% calcul du squelette
display 'calcul du squelette ...'
squel = squelette(imscene);

% élagage du squelette
display 'elagage du squelette ...'
%squel = elagage(squel, imscene, accessibilite);

% raccordement du depart et de l'arrivee
display 'raccordement du depart et de l''arrivee ...'
%squel = raccordement(squel, depart, arrivee, accessibilite);
squel = raccordement(squel, depart, arrivee, imscene-7); % plus rapide pour mes tests

% post-squelettisation
display 'post-squelettisation ...'
squel = post_squel(squel);

% affichage
%figure; imagesc(squel - imscene + accessibilite); axis equal


% calcul du graphe
display 'calcul du graphe ...'
[g, squel, noeud_depart, noeud_arrivee] = squelette_to_graphe(squel, depart, arrivee);


% affichage du graphe
%figure; 
%imagesc(int16(graphe_im) + imscene ); 
%axis equal;

% Calcul du plus court chemin
display 'calcul du plus court chemin ...';
[chemin, g] = dijkstra(g, noeud_depart, noeud_arrivee);

%squel = colorise_chemin(squel, chemin);

fig = figure; 
datacursormode on;
imagesc(squel - imscene);
hold all
axis equal
dcm_obj = datacursormode(fig);
set(dcm_obj,'UpdateFcn',@cursor_graphe);

plot_graph(g, squel);


lignes = [];
for i=1:length(chemin)-1
    p = plot([chemin(i).pos(2), chemin(i+1).pos(2)], [chemin(i).pos(1), chemin(i+1).pos(1)]);
    set(p,'Color','green','LineWidth',3)
    hold all
end

