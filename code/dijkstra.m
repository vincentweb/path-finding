% g : graphe
% start : noeud de départ
% end : noeud d'arrivée

% WARNING !!!
% Dans un vrai langague (avec des tructures de données évoluées et où on peut construire des pointeurs à partir d'instance de classes)
% On n'aurait ni besoin de l'attribut indice, ni de "g", et le code serait
% nettement plus lisible

% noeud : struct('indice', 'pos', 'fils', 'distance')

% Précondition, champs distances de tous les noeuds init à \inf

function [chemin,g]=dijkstra(g, debut, fin)

    function [is_in] = node_in_list(node, list)
        is_in = false;
        for i=1:length(list)
            if list(i).pos == node.pos
                is_in = true;
            end
        end
    end
    
    function [nlist] = sort_by_dist(list)
        dists = [];
        for i = 1:length(list)
            dists = [dists list(i).distance];
        end
        [sorted, indices] = sort(dists);
        nlist = list(indices);
    end
            

    g(debut.indice).distance = 0; % toutes les autres doivent etre à l infini
    path = {};
    lTree = [debut];

    while length(lTree) > 0
        % mis à jour des distances et choix du chemin le + court
        %display('pere');
        inode  = lTree(1).indice; % node = ltree.pop(0)
        %g(inode)
        lTree = lTree(2:length(lTree));
        for i=1:length(g(inode).fils)
            %display('son');
            ison = g(inode).fils(i);
            dist = distance(g(ison).pos, g(inode).pos)+g(inode).distance;
            if dist <= g(ison).distance 
                %son.visite = 0;  % si un chemin vers un noeud est plus court, alors il faut ré-utiliser ce noeud
                g(ison).distance = dist; % dist = min(node.sons.distance, dist)
                path{g(ison).indice} = g(inode);
                
                %son
                %debut.fils(1)
                
                %[g(ison).indice, g(inode).indice, g(ison).distance, dist] %debut.distance, debut.fils(1).distance, debut.fils(2).distance, fin.distance]
                if ~ node_in_list(g(ison), lTree)
                    lTree = [lTree g(ison)]; % si distance pas interessante, sert à rien de le revisiter
                end
            end
        end 
        lTree = sort_by_dist(lTree);
    end
    
    chemin = [];
    node   = fin;
    while node.indice ~= debut.indice
        chemin = [node chemin];
        node = path{node.indice};
    end
    chemin = [debut chemin];
        
    
end