function [ squel ] = post_squel( squel )

nb_lig = size(squel, 1);
nb_col = size(squel, 2);

    function [ nb ] = calculer_voisins(squel, i, j)
       
        nb = ( squel(i-1,j-1) ~= 7 ) ...
           + ( squel(i-1,j  ) ~= 7 ) ...
           + ( squel(i-1,j+1) ~= 7 ) ...
           + ( squel(i  ,j-1) ~= 7 ) ...
           + ( squel(i  ,j+1) ~= 7 ) ...
           + ( squel(i+1,j-1) ~= 7 ) ...
           + ( squel(i+1,j  ) ~= 7 ) ...
           + ( squel(i+1,j+1) ~= 7 ) ;
        
    end

    function [ res ] = a_un_voisin_carrefour(squel, i, j)
       
        res = ( squel(i-1,j-1) == 4 ) ...
           || ( squel(i-1,j  ) == 4 ) ...
           || ( squel(i-1,j+1) == 4 ) ...
           || ( squel(i  ,j-1) == 4 ) ...
           || ( squel(i  ,j+1) == 4 ) ...
           || ( squel(i+1,j-1) == 4 ) ...
           || ( squel(i+1,j  ) == 4 ) ...
           || ( squel(i+1,j+1) == 4 ) ;
        
    end

% normalisation des 0 et 1
for i = 1:nb_lig
    for j = 1:nb_col
        if squel(i,j) == 1
            squel(i,j) = 0;
        end
    end
end

% calcul des extremites et des carrefours
for i = 1:nb_lig
    for j = 1:nb_col
        if squel(i,j) ~= 7
            nb_voisins = calculer_voisins(squel, i, j);

            if nb_voisins == 1 % extremite
                squel(i,j) = 3;
            elseif nb_voisins > 2 % carrefour
                squel(i,j) = 4;
            end
        end
    end
end

% todo : reduire les carrrefours 

% calculd des pixels 'complexes'
for i = 1:nb_lig
    for j = 1:nb_col
        if (squel(i,j) ~= 7) ...
        && (squel(i,j) ~= 4) ...
        && a_un_voisin_carrefour(squel, i, j)
            squel(i,j) = 2;
        end
    end
end

end

