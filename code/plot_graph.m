function  plot_graph(g)

    % Couleurs dispos : 
    % yellow, magenta, cyan, red, green, blue, white, black

    %nim = zeros(size(im));

    for i=1:length(g)
        g(i).visited = 0;
    end

    lTree = [g(1).indice];
    
    while ~isempty(lTree)
        inode  = lTree(1);
        lTree = lTree(2:length(lTree));
        g(inode).visited = 1;

        for i=1:length(g(inode).fils)

            if ~g(g(inode).fils(i)).visited
                %[x y] = bresenham(g(inode).pos(1), g(inode).pos(2), g(g(inode).fils(i)).pos(1), g(g(inode).fils(i)).pos(2));
                p = plot([g(inode).pos(2), g(g(inode).fils(i)).pos(2)], [g(inode).pos(1), g(g(inode).fils(i)).pos(1)]);
                set(p,'Color','black','LineWidth',3);
                hold all;
                
                %z = x + (y-1) * size(im,1);
                %nim(z) = 3;
                %nim(z(1)) = 5;
                %nim(z(length(z))) = 5;
                lTree = [lTree g(inode).fils(i)];
            end
        end
    end
    
return;